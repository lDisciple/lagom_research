1.Clone the personModel repo to your local system.
2.Open personModel in intelliJ and wait for everything to download.
3.Edit run/debug configurations to a sbt task and under the Tasks tab type: runAll
4.Apply changes and run the personModel.(Ignore the staticLoggerBinder warning)
5.When the services started message is recieved connnect to the localhost:9000.[info] (Services started, press enter to stop and go back to the console...)
6.Load people into the database by starting a terminal in the utilities directory and typing: python StreamsConsole.py < Data/Profiles.cmd
																			       companies: python StreamsConsole.py < Businesses.cmd
																			   relationships: python StreamsConsole.py < relationships.cmd
//It will take some time to populate the database

To view a person's info go to http://localhost:9000/api/person/*user hash*