from inputHandler import InputHandler
from colorText import ColorText
import urllib
import urllib.parse
import urllib.error
import urllib.request
import urllib.response
import json
import sys
import os
import time
import random

user = "Alice"
usersList = []
usersFile = open(os.getcwd() + "/Data/userIds.txt","r")
for u in usersFile:
	usersList.append(u.strip())

userdata = {
}
def setUser(name):
	global user
	user = name

def setRandomUser():
	setUser(usersList[random.randint(0,len(usersList)-1)])
	getInfo()

def updateUserdata():
	urlCheck = urllib.request.Request("http://localhost:9000/api/person/" + user.replace(" ","%20") + "/raw")
	data = urllib.request.urlopen(urlCheck).read().decode('utf8', 'ignore')
	print(data)
	attribs = data.split("~")
	for attrib in attribs:
		pair = attrib.split("=")
		if pair[0] == 'age':
			userdata[pair[0]] = int(pair[1])
		else:
			if pair[1] == 'Not set':
				pair[1] = ""
			userdata[pair[0]] = pair[1]

def getInfo():
	urlCheck = urllib.request.Request("http://localhost:9000/api/person/" + user.replace(" ","%20"))
	data = urllib.request.urlopen(urlCheck).read().decode('utf8', 'ignore')
	print(ColorText.getColoredText(data.strip(),color=ColorText.HEADER))

def addRelationship(name,relType):
	data = json.dumps({"personid":name,"relType":relType})
	url = urllib.request.Request("http://localhost:9000/api/person/" + user.replace(" ","%20")+"/relationships",data.encode())
	url.add_header("Content-Type","application/json")
	reply = urllib.request.urlopen(url, timeout=4)#Send POST
	print("reply:")
	print(reply.read().decode('utf8', 'ignore'))
	

def setInfo(quiet = True):
	data = json.dumps(userdata)
	url = urllib.request.Request("http://localhost:9000/api/person/" + user.replace(" ","%20"),data.encode())
	url.add_header("Content-Type","application/json")
	reply = urllib.request.urlopen(url, timeout=2)#Send POST
	time.sleep(0.002)
	if not quiet:
		getInfo()
	

def setAttrib(key, value,quiet = True):
	if key in userdata:
		if key == 'age':
			userdata[key] = int(value)			
		else:
			userdata[key] = value
	if not quiet:
		for key in userdata:
			print("%-20s%s" % (key,str(userdata[key]).replace("-1","")))

	

inp = InputHandler()
inp.start()
updateUserdata()

commands = {
		"info" : getInfo,
		"set" : setAttrib,
		"update" : setInfo,
		"random" : setRandomUser,
		"user" : setUser,
		"print" : print,
		"addRel" : addRelationship
	}
command = ""
isRunning = True
while isRunning:
	count = 0
	for cmnd in inp.getInput():
		cmndFull = cmnd.split("~")
		if cmndFull[0] in commands:
			tries = 5
			passed = true
			while not passed and tries > 0:
				try:
					commands[cmndFull[0]](*tuple(cmndFull[1:]))
					count+=1
					print("{} %".format(count/103.15))
				except:
					tries-=1
					passed = false
		if cmnd == "stop":
			isRunning = False

inp.stop()
print("Press Enter to quit")