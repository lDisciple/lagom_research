from inputHandler import InputHandler
from colorText import ColorText
import json
import sys
import os
import time
import random
import websocket
totalCommands = 0
if len(sys.argv)> 1:
	totalCommands = int(sys.argv[1])
userList = []
usersFile = open(os.getcwd() + "/userIds.txt","r")
for u in usersFile:
	userList.append(u.strip())
busList = []
busFile = open(os.getcwd() + "/busIds.txt","r")
for u in busFile:
	busList.append(u.strip())

def parseRawPersonData(data):
	userdata={}
	attribs = data.split("~")
	for attrib in attribs:
		pair = attrib.split("=")
		if pair[0] == 'age':
			userdata[pair[0]] = int(pair[1])
		else:
			
			if pair[1] == 'Not set':
				pair[1] = ""
			userdata[pair[0]] = pair[1]
	return userdata

def parseRawCompanyData(data):
	userdata={}
	attribs = data.split("~")
	for attrib in attribs:
		pair = attrib.split("=")
		if pair[0] == 'networth':
			userdata[pair[0]] = float(pair[1])
		else:
			if pair[1] == 'Not set':
				pair[1] = ""
			userdata[pair[0]] = pair[1]
	return userdata

ws = websocket.WebSocket()

def getPersonInfo(personid):
	data = json.dumps({'command':'GetPersonInfo','id':personid})
	ws.send(data.encode())
	return ws.recv()
def getRawPersonInfo(companyid):
	data = json.dumps({'command':'GetRawPersonInfo','id':companyid})
	ws.send(data.encode())
	return ws.recv()

def getCompanyInfo(companyid):
	data = json.dumps({'command':'GetCompanyInfo','id':companyid})
	ws.send(data.encode())
	return ws.recv()

def getRawCompanyInfo(companyid):
	data = json.dumps({'command':'GetRawCompanyInfo','id':companyid})
	ws.send(data.encode())
	return ws.recv()

def setPersonInfo(personid,*vargs):
	oldData = parseRawPersonData(getRawPersonInfo(personid))
	for arg in vargs:
		pair = arg.split("::")
		if pair[0] == 'age':
			pair[1] = int(pair[1])
		oldData[pair[0]] = pair[1]
	newdata = json.dumps({'command':'SetPersonInfo','id':personid,'personDetails':json.dumps(oldData)})
	ws.send(newdata.encode())
	return ws.recv()

def setCompanyInfo(companyid,*vargs):
	oldData = parseRawCompanyData(getRawCompanyInfo(companyid))
	for arg in vargs:
		pair = arg.split("::")
		if pair[0] == 'networth':
			pair[1] = float(pair[1])
		oldData[pair[0]] = pair[1]
	newdata = json.dumps({'command':'SetCompanyInfo','id':companyid,'companyDetails':json.dumps(oldData)})
	ws.send(newdata.encode())
	return ws.recv()

def employPerson(companyid,personid,position):
	data = json.dumps({'command':'EmployPerson','id':companyid,'personid':personid,'position':position})
	ws.send(data.encode())
	return ws.recv()	

def firePerson(companyid,personid,position):
	data = json.dumps({'command':'FirePerson','id':companyid,'personid':personid})
	ws.send(data.encode())
	return ws.recv()

def addRelationship(mainid,personid,relType):
	data = json.dumps({'command':'AddRelationship','id':mainid,'personid':personid,'relType':relType})
	ws.send(data.encode())
	return ws.recv()

def removeRelationship(mainid,personid):
	data = json.dumps({'command':'RemoveRelationship','id':mainid,'personid':personid})
	ws.send(data.encode())
	return ws.recv()

def viewRelationships(mainid):
	data = json.dumps({'command':'ViewRelationships','id':mainid})
	ws.send(data.encode())
	return ws.recv()

def getDegreeOfFriendship(mainid,friendid):
	data = json.dumps({'command':'GetRelationshipDegree','id':mainid,'personid':friendid})
	ws.send(data.encode())
	return ws.recv()

def randomCompany():
	company = busList[random.randint(0,len(busList)-1)]
	print(ColorText.getColoredText(company,ColorText.BOLD))
	return getCompanyInfo(company)

def randomPerson():
	person = userList[random.randint(0,len(userList)-1)]
	print(ColorText.getColoredText(person,ColorText.BOLD))
	return getPersonInfo(person)

inp = InputHandler()
inp.start()

commands = {
		"personInfo" : getPersonInfo,
		"companyInfo" : getCompanyInfo,
		"print" : print,
		"setPersonInfo" : setPersonInfo,
		"setCompanyInfo" : setCompanyInfo,
		"employ" : employPerson,
		"fire" : firePerson,
		"addRelationship" : addRelationship,
		"removeRelationship" : removeRelationship,
		"degree" : getDegreeOfFriendship,
		"viewRelationships" : viewRelationships,
		"randomCompany" : randomCompany,
		"randomPerson" : randomPerson
	}

ws.connect('ws://localhost:9000/stream')
command = ""
isRunning = True
count = 0
intervalCount = 0
intervals = 100
while isRunning:
	for cmnd in inp.getInput():
		cmndFull = cmnd.split("~")
		if cmndFull[0] in commands:
			tries = 5
			passed = False
			while (not passed) and tries > 0:
				#try:
					try:
						reply = commands[cmndFull[0]](*tuple(cmndFull[1:]))
						passed = True
						if totalCommands > 0:
							count+=1
							intervalCount += 1
							if intervalCount >= intervals:
								print("{:.2f} %".format(count*100.0/totalCommands))
								intervalCount = 0
						else:
							if not reply is None and len(str(reply)) > 0:
								print(ColorText.getColoredText(reply,ColorText.OKBLUE))
					except websocket._exceptions.WebSocketConnectionClosedException:
						ws.connect('ws://localhost:9000/stream')
						passed = False
						print("Socket was closed.")
				#except:
				#	tries-=1
				#	passed = False
				#	print("Error")

			
		if cmnd == "stop":
			isRunning = False

inp.stop()
ws.close()
print("Press Enter to quit")