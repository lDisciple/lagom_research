from inputHandler import InputHandler
from colorText import ColorText
import urllib
import urllib.parse
import urllib.error
import urllib.request
import urllib.response
import json
import sys
import os
import time
import random

user = "Zenaptix"
usersList = []
usersFile = open(os.getcwd() + "/Data/userIds.txt","r")
for u in usersFile:
	usersList.append(u.strip())

userdata = {
}
def setUser(name):
	global user
	user = name

def setRandomUser():
	setUser(usersList[random.randint(0,len(usersList)-1)])
	getInfo()

def updateUserdata():
	urlCheck = urllib.request.Request("http://localhost:9000/api/company/" + user.replace(" ","%20") + "/raw")
	data = urllib.request.urlopen(urlCheck).read().decode('utf8', 'ignore')
	print(data)
	attribs = data.split("~")
	for attrib in attribs:
		pair = attrib.split("=")
		if pair[0] == 'networth':
			userdata[pair[0]] = float(pair[1])
		else:
			if pair[1] == 'Not set':
				pair[1] = ""
			userdata[pair[0]] = pair[1]
		

def getInfo():
	urlCheck = urllib.request.Request("http://localhost:9000/api/company/" + user.replace(" ","%20"))
	data = urllib.request.urlopen(urlCheck).read().decode('utf8', 'ignore')
	print(ColorText.getColoredText(data.strip(),color=ColorText.HEADER))

def addEmployee(personid,position):
	data = json.dumps({"personid":personid,"position":position})
	url = urllib.request.Request("http://localhost:9000/api/company/" + user.replace(" ","%20")+"/employ",data.encode())
	url.add_header("Content-Type","application/json")
	reply = urllib.request.urlopen(url, timeout=4)#Send POST

def removeEmployee(personid):
	data = json.dumps({"personid":personid})
	url = urllib.request.Request("http://localhost:9000/api/company/" + user.replace(" ","%20")+"/fire",data.encode())
	url.add_header("Content-Type","application/json")
	reply = urllib.request.urlopen(url, timeout=4)#Send POST
	

def setInfo(quiet = True):
	global user
	print (user)
	data = json.dumps(userdata)
	url = urllib.request.Request("http://localhost:9000/api/company/" + user.replace(" ","%20"),data.encode())
	url.add_header("Content-Type","application/json")
	reply = urllib.request.urlopen(url, timeout=2)#Send POST
	time.sleep(0.002)
	if not quiet:
		getInfo()
	

def setAttrib(key, value,quiet = True):
	if key in userdata:
		if key == 'networth':
			userdata[key] = float(value)			
		else:
			userdata[key] = value
	if not quiet:
		for key in userdata:
			print("%-20s%s" % (key,str(userdata[key]).replace("-1","")))

	

inp = InputHandler()
inp.start()
getInfo()
updateUserdata()

commands = {
		"info" : getInfo,
		"set" : setAttrib,
		"update" : setInfo,
		"random" : setRandomUser,
		"user" : setUser,
		"print" : print,
		"employ" : addEmployee,
		"fire":removeEmployee
	}
command = ""
isRunning = True
while isRunning:
	for cmnd in inp.getInput():
		cmndFull = cmnd.split("~")
		if cmndFull[0] in commands:
			tries = 5
			try:
				commands[cmndFull[0]](*tuple(cmndFull[1:]))
			except:
				tries-=1
		if cmnd == "stop":
			isRunning = False

inp.stop()
print("Press Enter to quit")