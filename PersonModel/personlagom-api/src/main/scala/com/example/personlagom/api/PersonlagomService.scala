package com.example.personlagom.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.Service.{pathCall, topic}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.broker.kafka.{KafkaProperties, PartitionKeyStrategy}
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

object PersonlagomService  {
  val PERSON_UPDATES_TOPIC = "personUpdatesStore"
  val COMPANY_UPDATES_TOPIC = "companyUpdatesStore"
  val MARKET_TOPIC = "marketStore"
}

/**
  * The PersonLagom service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the PersonlagomService.
  */
trait PersonlagomService extends Service {
  def getPersonInfo(id: String) : ServiceCall[NotUsed,String]
  def getPersonInfoRaw(id: String) : ServiceCall[NotUsed,String]
  def getPersonCompanyHistory(id: String) : ServiceCall[NotUsed,String]
  def getPersonAddressHistory(id: String) : ServiceCall[NotUsed,String]
  def getPersonLocationHistory(id: String) : ServiceCall[NotUsed,String]
  def getPersonRelationships(id: String) : ServiceCall[NotUsed,String]
  def setPersonInfo(id: String) : ServiceCall[PersonDetails,Done]
  def addRelationship(id: String) : ServiceCall[PersonRelationship,Done]
  def removeRelationship(id: String) : ServiceCall[PersonID,Done]
  def employPerson(id: String) : ServiceCall[EmployeeDefinition,Done]
  def firePerson(id: String) : ServiceCall[PersonID,Done]
  def getCompanyInfo(id: String) : ServiceCall[NotUsed,String]
  def getCompanyInfoRaw(id: String) : ServiceCall[NotUsed,String]
  def setCompanyInfo(id: String) : ServiceCall[CompanyDetails,Done]
  def getDegreeOfRelationship(id: String) : ServiceCall[PersonID,String]



  /**
    * This gets published to Kafka.
    */
  def personUpdatesTopic(): Topic[EventDetails  ]
  def companyUpdatesTopic(): Topic[EventDetails]
  def marketTopic(): Topic[NetworthUpdate]

  override final def descriptor = {
    import Service._
    // @formatter:off
    named("personlagom")
      .withCalls(
        pathCall("/api/person/:id", getPersonInfo _),
        pathCall("/api/person/:id/raw", getPersonInfoRaw _),
        pathCall("/api/person/:id/companyHistory", getPersonCompanyHistory _),
        pathCall("/api/person/:id/locationHistory", getPersonLocationHistory _),
        pathCall("/api/person/:id/addressHistory", getPersonAddressHistory _),
        pathCall("/api/person/:id", setPersonInfo _),
        pathCall("/api/person/:id/relationships/rem", removeRelationship _),
        pathCall("/api/person/:id/relationships", addRelationship _),
        pathCall("/api/person/:id/relationships", getPersonRelationships _),
        pathCall("/api/company/:id", getCompanyInfo _),
        pathCall("/api/company/:id", setCompanyInfo _),
        pathCall("/api/company/:id/raw", getCompanyInfoRaw _),
        pathCall("/api/company/:id/employ", employPerson _),
        pathCall("/api/company/:id/fire", firePerson _),
        pathCall("/api/person/degree/:id", getDegreeOfRelationship _)
      ).withTopics(
      topic(PersonlagomService.MARKET_TOPIC, marketTopic _)
        .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[NetworthUpdate](_.companyid)
        ),
      topic(PersonlagomService.PERSON_UPDATES_TOPIC, personUpdatesTopic _)
      .addProperty(
        KafkaProperties.partitionKeyStrategy,
        PartitionKeyStrategy[EventDetails](_.time)
      ),
      topic(PersonlagomService.COMPANY_UPDATES_TOPIC, companyUpdatesTopic _)
        .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[EventDetails](_.time)
        )
    )
      .withAutoAcl(true)
    // @formatter:on
  }
}

////////////////////////////////////////////////
//                   Formats                  //
////////////////////////////////////////////////
/**
  * The personDetails message class used by the topic stream.
  * Different than [[PersonDetails]], this message includes the id of the entity.
  */
case class PersonDetailsSet(id: String,
                            idNum: String,
                            name: String,
                            address: String,
                            cell: String,
                            gender: String,
                            race: String,
                            email:String,
                            location:String,
                            age: Int)
object PersonDetailsSet {
  implicit val format: Format[PersonDetailsSet] = Json.format[PersonDetailsSet]
}

case class PersonRelationship(personid: String,relType:String)
object PersonRelationship {
  implicit val format: Format[PersonRelationship] = Json.format[PersonRelationship]
}

case class EmployeeDefinition(personid: String,position:String)
object EmployeeDefinition {
  implicit val format: Format[EmployeeDefinition] = Json.format[EmployeeDefinition]
}

case class PersonID(personid: String)
object PersonID {
  implicit val format: Format[PersonID] = Json.format[PersonID]
}

case class CompanyDetails(
                           name: String,
                           owner: String,
                           location: String,
                           telephone: String,
                           networth: Float)
object CompanyDetails {
  implicit val format: Format[CompanyDetails] = Json.format[CompanyDetails]
}

case class CompanyDetailsSet(
                           id:String,
                           name: String,
                           owner: String,
                           location: String,
                           telephone: String,
                           networth: Float)
object CompanyDetailsSet {
  implicit val format: Format[CompanyDetailsSet] = Json.format[CompanyDetailsSet]
}

case class PersonDetails(
                          idNum: String,
                          name: String,
                          address: String,//~ delimited
                          cell: String,
                          gender: String,
                          race: String,
                          email:String,
                          location:String,//~ delimited
                          age: Int)
object PersonDetails {
  implicit val format: Format[PersonDetails] = Json.format[PersonDetails]
}

case class NetworthUpdate(companyid: String, networth:Float)
object NetworthUpdate {
  implicit val format: Format[NetworthUpdate] = Json.format[NetworthUpdate]
}

case class EventDetails(eventDetails:String, time:String)
object EventDetails {
  implicit val format: Format[EventDetails] = Json.format[EventDetails]
}




