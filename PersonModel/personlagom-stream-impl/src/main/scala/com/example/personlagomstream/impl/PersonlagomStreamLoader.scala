package com.example.personlagomstream.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.example.personlagomstream.api.PersonlagomStreamService
import com.example.personlagom.api.PersonlagomService
import com.softwaremill.macwire._

class PersonlagomStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new PersonlagomStreamApplication(context) {
      override def serviceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new PersonlagomStreamApplication(context) with LagomDevModeComponents

  override def describeServices = List(
    readDescriptor[PersonlagomStreamService]
  )
}

abstract class PersonlagomStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with LagomServerComponents
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer = serverFor[PersonlagomStreamService](wire[PersonlagomStreamServiceImpl])

  // Bind the PersonlagomService client
  lazy val personlagomService = serviceClient.implement[PersonlagomService]
}
