package com.example.personlagomstream.impl
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.example.personlagomstream.api.PersonlagomStreamService
import com.example.personlagom.api._
import play.api.libs.json.{Format, JsValue, Json}

import scala.concurrent.Future

/**
  * Implementation of the PersonlagomStreamService.
  */
class PersonlagomStreamServiceImpl(personlagomService: PersonlagomService) extends PersonlagomStreamService {
  def stream = ServiceCall { hellos =>
    def handleStreamEvent(commandString:String) ={
      val jsonObj = Json.parse(commandString)
      val command:StreamCommand = jsonObj.as[StreamCommand]
      command.command match{
        case "GetPersonInfo" => personlagomService.getPersonInfo(command.id).invoke()
        case "GetCompanyInfo" => personlagomService.getCompanyInfo(command.id).invoke()
        case "GetRawPersonInfo" => personlagomService.getPersonInfoRaw(command.id).invoke()
        case "GetRawCompanyInfo" => personlagomService.getCompanyInfoRaw(command.id).invoke()
        case "SetPersonInfo" =>
          val personDetails = Json.parse((jsonObj \ "personDetails").as[String]).as[PersonDetails]
          personlagomService.setPersonInfo(command.id).invoke(personDetails)
          Future.successful("Done")
        case "SetCompanyInfo" =>
          val companyDetails = Json.parse((jsonObj \ "companyDetails").as[String]).as[CompanyDetails]
          personlagomService.setCompanyInfo(command.id).invoke(companyDetails)
          Future.successful("Done")
        case "EmployPerson" =>
          val employmentDetails = jsonObj.as[EmployeeDefinition]
          personlagomService.employPerson(command.id).invoke(employmentDetails)
          Future.successful("Done")
        case "FirePerson" =>
          val personDetails = jsonObj.as[PersonID]
          personlagomService.firePerson(command.id).invoke(personDetails)
          Future.successful("Done")
        case "AddRelationship" =>
          val relDetails = jsonObj.as[PersonRelationship]
          personlagomService.addRelationship(command.id).invoke(relDetails)
          Future.successful("Done")
        case "RemoveRelationship" =>
          val personDetails = jsonObj.as[PersonID]
          personlagomService.removeRelationship(command.id).invoke(personDetails)
          Future.successful("Done")
        case "GetRelationshipDegree" =>
          val personDetails = jsonObj.as[PersonID]
          personlagomService.getDegreeOfRelationship(command.id).invoke(personDetails)
        case "ViewRelationships" =>
          personlagomService.getPersonRelationships(command.id).invoke()
      }
    }
    Future.successful(hellos.mapAsync(8)(handleStreamEvent(_)))
  }

  case class StreamCommand(command:String,id:String)
  object StreamCommand{
    implicit val format:Format[StreamCommand] = Json.format
  }
}
