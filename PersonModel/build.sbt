organization in ThisBuild := "com.example"
version in ThisBuild := "1.0-SNAPSHOT"
lagomKafkaEnabled := true
lagomKafkaPort in ThisBuild := 9092
lagomKafkaZookeperPort in ThisBuild := 9999
// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.8"

val macwire = "com.softwaremill.macwire" %% "macros" % "2.2.5" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1" % Test

lazy val `personlagom` = (project in file("."))
  .aggregate(`personlagom-api`, `personlagom-impl`, `personlagom-stream-api`, `personlagom-stream-impl`)

lazy val `personlagom-api` = (project in file("personlagom-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `personlagom-impl` = (project in file("personlagom-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(`personlagom-api`)

lazy val `personlagom-stream-api` = (project in file("personlagom-stream-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `personlagom-stream-impl` = (project in file("personlagom-stream-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`personlagom-stream-api`, `personlagom-api`)
