package com.example.personlagom.impl

import java.time.LocalDateTime
import java.util

import akka.Done
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import play.api.libs.json._

import scala.collection.mutable.HashMap
import scala.collection.immutable.Seq
import scala.collection.mutable
/**
  * This is an event sourced entity. It has a state, [CompanyState, which
  * stores what the greeting should be (eg, "Hello").
  *
  * Event sourced entities are interacted with by sending them commands. This
  * entity supports two commands, a [[SetCompanyInfo]] command, which is
  * used to change the info of the person, and a [[GetCompanyInfo]] command, which is a read
  * only command which returns a the info of the person specified by the command.
  *
  * Commands get translated to events, and it's the events that get persisted by
  * the entity. Each event will have an event handler registered for it, and an
  * event handler simply applies an event to the current state. This will be done
  * when the event is first created, and it will also be done when the entity is
  * loaded from the database - each event will be replayed to recreate the state
  * of the entity.
  */
class CompanyEntity extends PersistentEntity {

  override type Command = CompanyCommand[_]
  override type Event = CompanyEvent
  override type State = CompanyState

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  override def initialState: CompanyState = CompanyState(
    "Not set",
    "Not set",
    null,
    "Not set",
    new HashMap,
    0,
    LocalDateTime.now.toString)

  /**
    * An entity can define different behaviours for different states, so the behaviour
    * is a function of the current state to a set of actions.
    */
  override def behavior: Behavior = {
    case CompanyState(name, owner, location, telephone, employees, networth, _) => Actions().onCommand[SetCompanyInfo, Done] {

      // Command handler for the UseGreetingMessage command
      case (SetCompanyInfo(name, owner, location, telephone, networth), ctx, state) =>
        ctx.thenPersist(
          CompanyInfoChanged(name, owner, location, telephone, networth)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onCommand[EmployPerson, Done] {
      case (EmployPerson(personID: String, position: String), ctx, state) =>
        ctx.thenPersist(
          PersonEmployed(personID, position)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onCommand[FirePerson, Done] {
      case (FirePerson(personID: String), ctx, state) =>
        ctx.thenPersist(
          PersonFired(personID)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onReadOnlyCommand[GetCompanyInfo, String] {
      case (GetCompanyInfo(rawOut), ctx, state) =>
        var reply = ""
        if (rawOut) {
          //Setup raw data display
          //Get last location
          var locationString = ""
          if (location != null && location.valid) {
            val locationTuple = location
            locationString = locationTuple.address + "," + locationTuple.lat + "," + locationTuple.lon
          }
          var employeeString: String = ""
          val empKeys = employees.keysIterator
          for (x <- 0 until employees.size) {
            val empKey = empKeys.next()
            employeeString += empKey + ":" + employees(empKey) + "&&"
          }
          if (!employeeString.isEmpty()) {
            employeeString = employeeString.substring(0, employeeString.length - 1)
          }
          reply = s"name=$name~owner=$owner~location=$locationString~telephone=$telephone~employees=$employeeString~networth=$networth"
        } else {
          //Display structured data
          //Get last location
          var locationString = ""
          if (location != null && location.valid) {
            val locationTuple = location
            locationString = locationTuple.address + "," + locationTuple.lat + "," + locationTuple.lon
          }
          var employeeString: String = ""
          val empKeys = employees.keysIterator
          for (x <- 0 until employees.size) {
            val empKey = empKeys.next()
            employeeString += "\n" + empKey + ": " + employees(empKey)
          }
          reply = s"Name:\t\t$name" + "\n" +
            s"Address:\t$locationString" + "\n" +
            s"Tel:\t\t$telephone" + "\n" +
            s"Owner:\t\t$owner" + "\n" +
            s"Net worth:\t$networth" + "\n" +
            s"Employees:\t$employeeString" + "\n"
        }
        ctx.reply(reply)
    }.onEvent {
      case (PersonFired(personID:String), state) =>
        employees.remove(personID)
        CompanyState(name, owner, location, telephone, employees, networth, LocalDateTime.now().toString)
    }.onEvent {
      case (PersonEmployed(personID:String,position:String), state) =>
        employees.put(personID,position)
        CompanyState(name, owner, location, telephone, employees, networth, LocalDateTime.now().toString)
    }.onEvent {
      case (CompanyInfoChanged(newName, newOwner, newLocation, newTelephone, newNetworth), state) =>
        var finalName = name
        if (name.length > 0){
          finalName = newName
        }
        var finalOwner = owner
        if (newOwner.length > 0){
          finalOwner = newOwner
        }
        var finalTelephone = telephone
        if (newTelephone.length > 0){
          finalTelephone = newTelephone
        }
        var finalNetworth = networth
        if (newNetworth > 0){
          finalNetworth = newNetworth
        }
        var finalLocation = location
        if (newLocation.valid){
          finalLocation = newLocation
        }
        CompanyState(finalName, finalOwner, finalLocation, finalTelephone, employees, finalNetworth, LocalDateTime.now().toString)
    }
  }

}
  //////////////////////////////////////////////
  //      State/Event/Command Definitions     //
  //////////////////////////////////////////////

  //Event
  sealed trait CompanyEvent extends AggregateEvent[CompanyEvent] {
    def aggregateTag = CompanyEvent.Tag
  }

  /**
    * This interface defines all the events that the PersonlagomEntity supports.
    */
  object CompanyEvent {
    val Tag = AggregateEventTag[CompanyEvent]
  }

  //Command
  /**
    * This interface defines all the commands that the HelloWorld entity supports.
    */
  sealed trait CompanyCommand[R] extends ReplyType[R]

  //State
  /**
    * The current state held by the persistent entity.
    */
  case class CompanyState(
                           name: String,
                           owner: String,
                           location: Location,
                           telephone: String,
                           employees: HashMap[String, String],
                           networth: Float,
                           timestamp: String)

  object CompanyState {
    implicit val hashMapFormat: Format[mutable.HashMap[String,String]] = HashMapFormat
    implicit val format: Format[CompanyState] = Json.format[CompanyState]
  }

  //////////////////////////////////////////////
  //            Format Definitions            //
  //////////////////////////////////////////////
  object CompanyFormats {
    val serializers: Seq[JsonSerializer[_]] = Seq(
      JsonSerializer[GetCompanyInfo],
      JsonSerializer[SetCompanyInfo],
      JsonSerializer[CompanyInfoChanged],
      JsonSerializer[EmployPerson],
      JsonSerializer[PersonEmployed],
      JsonSerializer[FirePerson],
      JsonSerializer[PersonFired]
    )
  }

  case class GetCompanyInfo(rawOut: Boolean) extends CompanyCommand[String]
  object GetCompanyInfo {
    implicit val format: Format[GetCompanyInfo] = Json.format[GetCompanyInfo]
  }

  case class SetCompanyInfo(name: String, owner: String, location: Location, telephone: String, networth: Float) extends CompanyCommand[Done]
  object SetCompanyInfo {
    implicit val format: Format[SetCompanyInfo] = Json.format[SetCompanyInfo]
  }

  case class CompanyInfoChanged(name: String, owner: String, location: Location, telephone: String, networth: Float) extends CompanyEvent
  object CompanyInfoChanged {
    implicit val format: Format[CompanyInfoChanged] = Json.format[CompanyInfoChanged]
  }

  case class PersonEmployed(personID: String,position: String) extends CompanyEvent
  object PersonEmployed {
    implicit val format: Format[PersonEmployed] = Json.format[PersonEmployed]
  }

  case class EmployPerson(personID: String,position: String) extends CompanyCommand[Done]
  object EmployPerson {
    implicit val format: Format[EmployPerson] = Json.format[EmployPerson]
  }

  case class FirePerson(personID: String) extends CompanyCommand[Done]
  object FirePerson {
    implicit val format: Format[FirePerson] = Json.format[FirePerson]
  }

  case class PersonFired(personID: String) extends CompanyEvent
  object PersonFired {
    implicit val format: Format[PersonFired] = Json.format[PersonFired]
  }
 object HashMapFormat extends Format[mutable.HashMap[String,String]] {
  override def reads(json: JsValue) = {
    val keys:Array[String] =((json \ "~~keys").as[String]).split("~")
    val map = new mutable.HashMap[String,String]
    for (key <- keys){
      map.put(key,(json \ key).as[String])
    }
    JsSuccess(map)
  }

  override def writes(map: mutable.HashMap[String,String]) = {
    var keys:String = ""
    val pairs:util.ArrayList[(String,JsValue)] = new util.ArrayList()
    for (key <- map.keySet){
      keys+="~"+key
      pairs.add(key -> Json.toJson(map.getOrElse(key,"")))
    }
    pairs.add(0,"~~keys" -> JsString(keys.substring(math.min(1,keys.length))))
    val seq:Seq[(String,JsValue)] = collection.immutable.Seq(pairs.toArray(new Array[(String,JsValue)](0)).toSeq:_*)
    JsObject(seq)
  }
}