package com.example.personlagom.impl
import scala.reflect.ClassTag
import java.util
import scala.reflect.runtime.{ universe => ru }
import com.lightbend.lagom.scaladsl.playjson.JsonSerializer
import play.api.libs.json._


import scala.collection.immutable.Seq
import scala.collection.mutable

case class Relationship(relationshipType: String, firstLog: String)
object Relationship {
  implicit val format: Format[Relationship] = Json.format[Relationship]
}
case class Location(lat: Float, lon: Float, address: String, valid:Boolean = true)
object Location {
  implicit val format: Format[Location] = Json.format[Location]

  def StringToLocation: String => Location = { locString =>
    val locParts:Array[String] = locString.split("@")
    if(locString.length > 0 && locParts.length == 3){
      val addressLocation:Location = new Location(locParts(0).toFloat,locParts(1).toFloat,locParts(2))
      addressLocation
    }else{
      new Location(0,0,"Invalid place",valid=false)
    }

  }
  def LocationToString: Location => String = { location =>
    location.lat + "@" + location.lon + "@" + location.address
  }
}

object UtilFormats{
  val serializers:Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[Relationship],
    JsonSerializer[Location]
  )
}
