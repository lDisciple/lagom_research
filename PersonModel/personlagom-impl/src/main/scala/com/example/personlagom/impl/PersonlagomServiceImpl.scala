package com.example.personlagom.impl

import java.time.LocalDateTime

import com.example.personlagom.api
import com.example.personlagom.api.PersonlagomService
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry}
import scala.concurrent.duration._
import scala.collection.mutable
import scala.concurrent.{Await, Future}

/**
  * Implementation of the PersonlagomService.
  */
class PersonlagomServiceImpl(persistentEntityRegistry: PersistentEntityRegistry) extends PersonlagomService {
  ////////////////////////////////////////////////
  //                 Person Input               //
  ////////////////////////////////////////////////
  override def getPersonInfo(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(GetPersonInfo(false))
  }
  override def getPersonInfoRaw(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(GetPersonInfo(true))
  }
  override def setPersonInfo(id: String) = ServiceCall { request =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(SetPersonInfo(
      request.idNum,
      request.name,
      Location.StringToLocation(request.address),
      request.cell,
      request.gender,
      request.race,
      request.email,
      Location.StringToLocation(request.location),
      request.age
    ))
  }
  override def getPersonLocationHistory(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(GetPersonLocationHistory())
  }
  override def getPersonAddressHistory(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(GetPersonAddressHistory())
  }
  override def getPersonCompanyHistory(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(GetPersonCompanyHistory())
  }
  override def getPersonRelationships(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PersonEntity](id)
    ref.ask(GetPersonRelationships(id))
  }
  override def addRelationship(id: String) = ServiceCall { req =>
    val ref1 = persistentEntityRegistry.refFor[PersonEntity](id)
    ref1.ask(AddRelationship(req.personid,req.relType))
    val ref2 = persistentEntityRegistry.refFor[PersonEntity](req.personid)
    ref2.ask(AddRelationship(id,req.relType))
  }
  override def removeRelationship(id: String) = ServiceCall { req =>
    val ref1 = persistentEntityRegistry.refFor[PersonEntity](id)
    ref1.ask(RemoveRelationship(req.personid))
    val ref2 = persistentEntityRegistry.refFor[PersonEntity](req.personid)
    ref2.ask(RemoveRelationship(id))
  }
  override def employPerson(id: String) = ServiceCall { req =>
    val refCompany = persistentEntityRegistry.refFor[CompanyEntity](id)
    refCompany.ask(EmployPerson(req.personid,req.position))
    val refPerson = persistentEntityRegistry.refFor[PersonEntity](req.personid)
    refPerson.ask(AddCompany(id))
  }
  override def firePerson(id: String) = ServiceCall { req =>
    val refPerson = persistentEntityRegistry.refFor[PersonEntity](req.personid)
    refPerson.ask(RemoveCompany(LocalDateTime.now().toString()))
    val refCompany = persistentEntityRegistry.refFor[CompanyEntity](id)
    refCompany.ask(FirePerson(req.personid))
  }
  override def getCompanyInfo(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[CompanyEntity](id)
    ref.ask(GetCompanyInfo(false))
  }
  override def getCompanyInfoRaw(id: String) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[CompanyEntity](id)
    ref.ask(GetCompanyInfo(true))
  }
  override def setCompanyInfo(id: String) = ServiceCall { req =>
    val ref = persistentEntityRegistry.refFor[CompanyEntity](id)
    ref.ask(SetCompanyInfo(req.name, req.owner, Location.StringToLocation(req.location), req.telephone, req.networth))
  }
  override def getDegreeOfRelationship(id: String) = ServiceCall { req =>
    case class RelationshipPath(previous:mutable.LinkedHashSet[String],degree:Int)
    var found:Boolean = false
    val name1Command = persistentEntityRegistry.refFor[PersonEntity](req.personid).ask(GetPersonName())
    val name2Command = persistentEntityRegistry.refFor[PersonEntity](id).ask(GetPersonName())
    var name1 = Await.result(name1Command, 2 seconds)
    var name2 = Await.result(name2Command, 2 seconds)
    val backMap:mutable.HashMap[String,RelationshipPath] = new mutable.HashMap()
    val frontMap:mutable.HashMap[String,RelationshipPath] = new mutable.HashMap()
    var answer:String = s"$name1(${req.personid}) and $name2($id) have no degree of friendship"
    val frontQueue = new mutable.Queue[String]()
    frontQueue.enqueue(id)
    frontMap(id) = new RelationshipPath(new mutable.LinkedHashSet[String],0)
    val backQueue = new mutable.Queue[String]()
    backQueue.enqueue(req.personid)
    backMap(req.personid) = new RelationshipPath(new mutable.LinkedHashSet[String],0)
    while(!found && !frontQueue.isEmpty && !backQueue.isEmpty){
      val frontPerson = frontQueue.dequeue()
      val frontPath = frontMap(frontPerson)
      val friendsCommand = persistentEntityRegistry.refFor[PersonEntity](frontPerson).ask(GetRelationshipArray())
      for (friend <- Await.result(friendsCommand, 2 seconds)){
        //Put Front elements in
        if(!frontMap.contains(friend)){
          frontQueue.enqueue(friend)
          val newList:mutable.LinkedHashSet[String] = frontPath.previous+frontPerson
          frontMap(friend) = new RelationshipPath(newList,frontPath.degree+1)
        }
      }

      val backPerson = backQueue.dequeue()
      val backPath = backMap(backPerson)
      val backFriendsCommand = persistentEntityRegistry.refFor[PersonEntity](backPerson).ask(GetRelationshipArray())
      for (friend <- Await.result(backFriendsCommand, 2 seconds)){
        //Put Front elements in
        if(!backMap.contains(friend)){
          backQueue.enqueue(friend)
          val newList:mutable.LinkedHashSet[String] = backPath.previous+backPerson
          backMap(friend) = new RelationshipPath(newList,backPath.degree+1)
        }
      }
      val common= frontMap.keySet.filter(x => backMap.keySet.contains(x))
      if(!common.isEmpty){
        found = true
        var smallest:Int = frontMap(common.head).degree+backMap(common.head).degree+10
        for (commonElement <- common){
          val degree:Int = frontMap(commonElement).degree+backMap(commonElement).degree-1
          if(degree < smallest){
            var finalPath:mutable.LinkedHashSet[String]  = (frontMap(commonElement).previous+commonElement)
            finalPath = finalPath++(backMap(commonElement).previous.toList.reverse)
            smallest=degree
            answer = s"$name1(${req.personid}) and $name2($id)'s relationship is degree $degree\nPath:"
            for(userId<-finalPath){

              val nameCommand = persistentEntityRegistry.refFor[PersonEntity](userId).ask(GetPersonName())
              var name = Await.result(nameCommand, 2 seconds)
              answer +=s"\n\t$name ($userId)"
            }
          }
        }
      }
      //println(frontMap)
      //println(backMap)
    }
      Future.successful(answer)
  }


  ////////////////////////////////////////////////
  //               Company Inputs               //
  ////////////////////////////////////////////////

  override def personUpdatesTopic(): Topic[api.EventDetails] =
    TopicProducer.singleStreamWithOffset {
      fromOffset =>
        persistentEntityRegistry.eventStream(PersonEvent.Tag, fromOffset)
          .map(ev => (convertEventToDetails(ev), ev.offset))
    }

  override def companyUpdatesTopic(): Topic[api.EventDetails] =
    TopicProducer.singleStreamWithOffset {
      fromOffset =>
        persistentEntityRegistry.eventStream(CompanyEvent.Tag, fromOffset)
          .map(ev => (convertEventToDetails(ev), ev.offset))
    }

  override def marketTopic(): Topic[api.NetworthUpdate] =
    TopicProducer.singleStreamWithOffset {
      fromOffset =>
        persistentEntityRegistry.eventStream(CompanyEvent.Tag, fromOffset)
          .map(ev => (convertCompanyEvent(ev), ev.offset))
    }

  private def convertEventToDetails(compEvent: EventStreamElement[_]): api.EventDetails = {
    val details = compEvent.entityId + ": " +compEvent.event
    api.EventDetails(details,LocalDateTime.now().toString)
  }


  private def convertCompanyEvent(compEvent: EventStreamElement[CompanyEvent]): api.NetworthUpdate = {
    compEvent.event match {
      case CompanyInfoChanged(_,_,_,_,networth) =>
        api.NetworthUpdate(compEvent.entityId,networth)
    }
  }
}
