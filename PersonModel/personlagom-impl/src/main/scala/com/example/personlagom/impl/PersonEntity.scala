package com.example.personlagom.impl

import java.lang.Math
import java.time.LocalDateTime
import java.{lang, util}

import scala.collection.mutable.LinkedHashMap
import akka.Done
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import play.api.libs.json._

import scala.collection.immutable.Seq
import scala.collection.mutable
import scala.collection.immutable
import scala.math

/**
  * This is an event sourced entity. It has a state, [[PersonState]], which
  * stores what the greeting should be (eg, "Hello").
  *
  * Event sourced entities are interacted with by sending them commands. This
  * entity supports two commands, a [[SetPersonInfo]] command, which is
  * used to change the info of the person, and a [[GetPersonInfo]] command, which is a read
  * only command which returns a the info of the person specified by the command.
  *
  * Commands get translated to events, and it's the events that get persisted by
  * the entity. Each event will have an event handler registered for it, and an
  * event handler simply applies an event to the current state. This will be done
  * when the event is first created, and it will also be done when the entity is
  * loaded from the database - each event will be replayed to recreate the state
  * of the entity.
  */
class PersonEntity extends PersistentEntity {

  override type Command = PersonCommand[_]
  override type Event = PersonEvent
  override type State = PersonState

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  override def initialState: PersonState = PersonState(
    "Not set",
    "Not set",
    new Array(0),
    new Array(0),
    "Not set",
    "Not set",
    "Not set",
    "Not set",
    -1,
    new mutable.HashMap,
    new Array(0),
    LocalDateTime.now.toString)

  /**
    * An entity can define different behaviours for different states, so the behaviour
    * is a function of the current state to a set of actions.
    */
  override def behavior: Behavior = {
    case PersonState(idNum, name, addressList, companyList, cell, gender, race, email, age, relationshipsMap, locations, _) => Actions().onCommand[SetPersonInfo, Done] {
      case (SetPersonInfo(idNum, name, address, cell, gender, race, email, age, location), ctx, state) =>
        ctx.thenPersist(
          PersonInfoChanged(idNum, name, address, cell, gender, race, email, age, location)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
      //Print out the info of person
    }.onCommand[AddCompany, Done] {
      case (AddCompany(compID: String), ctx, state) =>
        ctx.thenPersist(
          CompanyAdded(compID)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onCommand[AddRelationship, Done] {
      case (AddRelationship(personID: String, relID: String), ctx, state) =>
        ctx.thenPersist(
          RelationshipAdded(personID, relID)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onCommand[RemoveCompany, Done] {
      case (RemoveCompany(_), ctx, state) =>
        ctx.thenPersist(
          CompanyRemoved(LocalDateTime.now().toString)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onCommand[RemoveRelationship, Done] {
      case (RemoveRelationship(personID: String), ctx, state) =>
        ctx.thenPersist(
          RelationshipRemoved(personID)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }
    }.onReadOnlyCommand[GetPersonInfo, String] {
      case (GetPersonInfo(rawOut), ctx, state) =>
        var ageTag: String = "not set"
        if (age != -1) {
          ageTag = "" + age + " years old"
        }
        var reply = ""
        if (rawOut) {
          //Setup raw data display
          //Get last company
          var companyString = ""
          if (companyList.size > 0) {
            companyString = companyList(0)
          }
          //Get last location
          var locationString = ""
          if (locations.length > 0) {
            val locationLocation = locations(0)
            locationString = Location.LocationToString(locationLocation)
          }
          //Get last address
          var addressString = ""
          if (addressList.length > 0) {
            val addressLoation = addressList(0)
            addressString = Location.LocationToString(addressLoation)
          }
          reply = s"idNum=$idNum~name=$name~age=$age~cell=$cell~address=$addressString" +
            s"~email=$email~race=$race~gender=$gender~company=$companyString~location=$locationString"

        } else {
          //Display structured data
          var companyString = ""
          if (companyList.size > 0) {
            companyString = companyList(0)
          }
          //Get last location
          var locationString = ""
          if (locations.length > 0) {
            val locationTuple = locations(locations.length - 1)
            locationString = locationTuple.address + " (" + locationTuple.lat + ":" + locationTuple.lon + ")"
          }
          //Get last address
          var addressString = ""
          if (addressList.length > 0) {
            val addressTuple = addressList(addressList.length - 1)
            addressString = addressTuple.address + " (" + addressTuple.lat + ":" + addressTuple.lon + ")"
          }
          reply = s"Name:\t\t$name" + "\n" +
            s"ID Number:\t$idNum" + "\n" +
            s"Age:\t\t$ageTag" + "\n" +
            s"Gender:\t\t$gender" + "\n" +
            s"Race:\t\t$race" + "\n" +
            s"Cellphone:\t$cell" + "\n" +
            s"Email:\t\t$email\n" +
            s"Address:\t$addressString" + "\n" +
            s"Last Location:\t$locationString\n" +
            s"Company: $companyString\n"
        }

        ctx.reply(reply)
    }.onReadOnlyCommand[GetPersonCompanyHistory, String] {
      case (GetPersonCompanyHistory(), ctx, state) =>
        var companyString:String ="Company History:\n"
        for ( company <- companyList){
          companyString+=company+"\n"
        }
        ctx.reply(companyString)
    }.onReadOnlyCommand[GetPersonAddressHistory, String] {
      case (GetPersonAddressHistory(), ctx, state) =>
        var addressString:String ="Address History:\n"
        for ( address <- addressList){
          addressString+=address+"\n"
        }
        ctx.reply(addressString)
    }.onReadOnlyCommand[GetPersonLocationHistory, String] {
      case (GetPersonLocationHistory(), ctx, state) =>
        var locationString:String ="Location History:\n"
        for ( location <- locations){
          locationString+=location+"\n"
        }
        ctx.reply(locationString)
    }.onReadOnlyCommand[GetPersonRelationships, String] {
      case (GetPersonRelationships(id), ctx, state) =>
        var locationString:String ="Relationships:\n"
        for ( relKey <- relationshipsMap.keySet){
          val relValue:Relationship = relationshipsMap(relKey)
          locationString+=s"$relKey: ${relValue.relationshipType} [${relValue.firstLog}]\n"
        }
        ctx.reply(locationString)
    }.onReadOnlyCommand[GetRelationshipArray, collection.Set[String]] {
      case (GetRelationshipArray(), ctx, state) =>
        ctx.reply(state.relationships.keySet)
    }.onReadOnlyCommand[GetPersonName, String] {
      case (GetPersonName(), ctx, state) => ctx.reply(state.name)
    }.onEvent {
      case (PersonInfoChanged(newIDNum: String,
      newName: String,
      newAddress: Location,
      newCell: String,
      newGender: String,
      newRace: String,
      newEmail: String,
      newLocation: Location,
      newAge: Int), state) =>
        var finalName, finalCell, finalGender, finalRace, finalEmail, finalID: String = ""
        var finalAddressList: Array[Location] = new Array(0)
        var finalLocationList: Array[Location] = new Array(0)
        var finalAge: Int = 0
        if (newName != null && newName.length() > 0 && !newName.contains("~") && !newName.contains("=")) {
          finalName = newName
        } else {
          finalName = name
        }
        //Address List
        if (newAddress.valid &&
          newAddress.address.length > 0 &&
          (addressList.length == 0 ||
          !newAddress.address.equalsIgnoreCase(addressList(0).address))
        ) {
          finalAddressList = newAddress +: addressList
        }else{
          finalAddressList = addressList
        }
        //Location List
        if (newLocation.valid &&
          newLocation.address.length > 0 &&
          (locations.length == 0 ||
          !newLocation.address.equalsIgnoreCase(locations(0).address))
        ) {
          finalLocationList = newLocation +: locations
        }else{
          finalLocationList = locations
        }
        if (newIDNum != null && newIDNum.length() == 11 && !newIDNum.contains("~") && !newIDNum.contains("=")) {
          finalID = newIDNum
        } else {
          finalID = idNum
        }
        if (newEmail != null && newEmail.length() > 0 && !newEmail.contains("~") && !newEmail.contains("=")) {
          finalEmail = newEmail
        } else {
          finalEmail = email
        }
        if (newGender != null && newGender.length() > 0 && !newGender.contains("~") && !newGender.contains("=")) {
          finalGender = newGender
        } else {
          finalGender = gender
        }
        if (newCell != null && newCell.length() > 0 && !newCell.contains("~") && !newCell.contains("=")) {
          finalCell = newCell
        } else {
          finalCell = cell
        }
        if (newAge > 0 && age < 110) {
          finalAge = newAge
        } else {
          finalAge = age
        }
        if (newRace != null && newRace.length() > 0 && !newRace.contains("~") && !newRace.contains("=")) {
          finalRace = newRace
        } else {
          finalRace = race
        }
        PersonState(finalID, finalName, finalAddressList, companyList, finalCell, finalGender, finalRace, finalEmail, finalAge, relationshipsMap, finalLocationList, LocalDateTime.now().toString)

    }.onEvent {
      case (RelationshipAdded(personID: String,
      relType: String), state) =>
        if (!relationshipsMap.contains(personID)) {
          relationshipsMap.put(personID, new Relationship(relType, LocalDateTime.now().toString))
        } else {
          val date = relationshipsMap(personID).firstLog
          relationshipsMap.put(personID, new Relationship(relType, date))
        }
        PersonState(idNum, name, addressList, companyList, cell, gender, race, email, age, relationshipsMap, locations, LocalDateTime.now().toString)
    }.onEvent {
      case (RelationshipRemoved(personID: String), state) =>
        if (relationshipsMap.contains(personID)) {
          relationshipsMap.remove(personID)
        }
        PersonState(idNum, name, addressList, companyList, cell, gender, race, email, age, relationshipsMap, locations, LocalDateTime.now().toString)
    }.onEvent {
      case (CompanyAdded(companyID: String), state) =>
        var finalCompanies = companyList
        if (companyList.length == 0 || companyList(0) != companyID) {
          finalCompanies = companyID +: companyList
        }
        PersonState(idNum, name, addressList, finalCompanies, cell, gender, race, email, age, relationshipsMap, locations, LocalDateTime.now().toString)
    }.onEvent {
      case (CompanyRemoved(_), state) =>
        var finalCompanies = companyList
        if (companyList.length == 0 || companyList(0) != "Unemployed") {
          finalCompanies = "Unemployed" +: companyList
        }
        PersonState(idNum, name, addressList, finalCompanies, cell, gender, race, email, age, relationshipsMap, locations, LocalDateTime.now().toString)
    }
  }
}

  //////////////////////////////////////////////
  //      State/Event/Command Definitions     //
  //////////////////////////////////////////////

  //Event
  sealed trait PersonEvent extends AggregateEvent[PersonEvent] {
    def aggregateTag = PersonEvent.Tag
  }

  /**
    * This interface defines all the events that the PersonlagomEntity supports.
    */
  object PersonEvent {
    val Tag = AggregateEventTag[PersonEvent]

  }

  //Command
  /**
    * This interface defines all the commands that the HelloWorld entity supports.
    */
  sealed trait PersonCommand[R] extends ReplyType[R]

  //State

  /**
    * The current state held by the persistent entity.
    */
  case class PersonState(
                          idNum: String,
                          name: String,
                          addressList: Array[Location],
                          companyList: Array[String],
                          cell: String,
                          gender: String,
                          race: String,
                          email: String,
                          age: Int,
                          relationships: mutable.HashMap[String, Relationship],
                          locations: Array[Location],
                          timestamp: String)

  object PersonState {
    //implicit val relationshipFormat: Format[Relationship] = RelationshipFormat
    implicit val hashMapFormat: Format[mutable.HashMap[String,Relationship]] = RelationshipMapFormat
    implicit val format: Format[PersonState] = Json.format[PersonState]
  }

  /**
    * An event that represents a change in greeting message.
    */


  //////////////////////////////////////////////
  //            Format Definitions            //
  //////////////////////////////////////////////
  object PersonFormats{
    val serializers:Seq[JsonSerializer[_]] = Seq(
      JsonSerializer[GetPersonInfo],
      JsonSerializer[SetPersonInfo],
      JsonSerializer[PersonInfoChanged],
      JsonSerializer[AddRelationship],
      JsonSerializer[RelationshipAdded],
      JsonSerializer[RemoveRelationship],
      JsonSerializer[RelationshipRemoved],
      JsonSerializer[AddCompany],
      JsonSerializer[CompanyAdded],
      JsonSerializer[RemoveCompany],
      JsonSerializer[CompanyRemoved]
    )
  }

  case class AddRelationship(personID: String, relType: String) extends PersonCommand[Done]
  object AddRelationship {
    implicit val format: Format[AddRelationship] = Json.format
  }
case class RelationshipAdded(personID: String, relType: String) extends PersonEvent
object RelationshipAdded {
  implicit val format: Format[RelationshipAdded] = Json.format
}
case class GetRelationshipArray() extends PersonCommand[collection.Set[String]]

  case class GetPersonRelationships(personID: String) extends PersonCommand[String]
  object GetPersonRelationships {
    implicit val format: Format[GetPersonRelationships] = Json.format[GetPersonRelationships]
  }

  case class RemoveRelationship(personID: String) extends PersonCommand[Done]

  object RemoveRelationship {
    implicit val format: Format[RemoveRelationship] = Json.format
  }

  case class RelationshipRemoved(personID: String) extends PersonEvent

  object RelationshipRemoved {
    implicit val format: Format[RelationshipRemoved] = Json.format
  }

  case class AddCompany(companyID: String) extends PersonCommand[Done]

  object AddCompany {
    implicit val format: Format[AddCompany] = Json.format
  }

  case class CompanyAdded(companyID: String) extends PersonEvent
  object CompanyAdded {
    implicit val format: Format[CompanyAdded] = Json.format[CompanyAdded]
  }

  case class RemoveCompany(date:String) extends PersonCommand[Done]
  object RemoveCompany {
    implicit val format: Format[RemoveCompany] = Json.format[RemoveCompany]
  }

  case class CompanyRemoved(date:String) extends PersonEvent
  object CompanyRemoved {
    implicit val format: Format[CompanyRemoved] = Json.format[CompanyRemoved]
  }

  case class SetPersonInfo(idNum: String,
                           name: String,
                           address: Location,
                           cell: String,
                           gender: String,
                           race: String,
                           email: String,
                           location: Location,
                           age: Int) extends PersonCommand[Done]

  object SetPersonInfo {
    implicit val format: Format[SetPersonInfo] = Json.format
  }

  //Info events split into individual traits
  case class PersonInfoChanged(idNum: String,
                               name: String,
                               address: Location,
                               cell: String,
                               gender: String,
                               race: String,
                               email: String,
                               location: Location,
                               age: Int) extends PersonEvent

  object PersonInfoChanged {
    implicit val format: Format[PersonInfoChanged] = Json.format
  }

case class GetPersonInfo(rawOut: Boolean) extends PersonCommand[String]
object GetPersonInfo {
  implicit val format: Format[GetPersonInfo] = Json.format
}

case class GetPersonCompanyHistory() extends PersonCommand[String]

case class GetPersonAddressHistory() extends PersonCommand[String]

case class GetPersonLocationHistory() extends PersonCommand[String]

case class GetPersonName() extends PersonCommand[String]

object RelationshipMapFormat extends Format[mutable.HashMap[String,Relationship]] {
  override def reads(json: JsValue) = {
    implicit val relationshipFormat: Format[Relationship] = RelationshipFormat
    val keys:Array[String] =((json \ "~~keys").as[String]).split("~")
    val map = new mutable.HashMap[String,Relationship]
    for (key <- keys){
      map.put(key,(json \ key).as[Relationship])
    }
    JsSuccess(map)
  }

  override def writes(map: mutable.HashMap[String,Relationship]) = {
    var keys:String = ""
    val pairs:util.ArrayList[(String,JsValue)] = new util.ArrayList()
    for (key <- map.keySet){
      keys+="~"+key
      pairs.add(key -> Json.toJson(key))
    }
    pairs.add(0,"~~keys" -> JsString(keys.substring(math.min(1,keys.length))))
    val seq:Seq[(String,JsValue)] = collection.immutable.Seq(pairs.toArray(new Array[(String,JsValue)](0)).toSeq:_*)
    JsObject(seq)
  }
}
object RelationshipFormat extends Format[Relationship] {
  override def reads(json: JsValue) = {
    JsSuccess(new Relationship((json \ "type").as[String],(json \ "date").as[String]))
  }

  override def writes(rel:Relationship) = {
    val seq = Seq(
      "type" -> JsString(rel.relationshipType),
      "date" -> JsString(rel.firstLog))
    JsObject(seq)
  }
}
