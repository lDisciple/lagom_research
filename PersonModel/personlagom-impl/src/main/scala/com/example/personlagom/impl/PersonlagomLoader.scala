package com.example.personlagom.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.example.personlagom.api.PersonlagomService
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.softwaremill.macwire._

import scala.collection.immutable.Seq

class PersonlagomLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new PersonlagomApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new PersonlagomApplication(context) with LagomDevModeComponents

  override def describeServices = List(
    readDescriptor[PersonlagomService]
  )
}

abstract class PersonlagomApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with CassandraPersistenceComponents
    with LagomKafkaComponents
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer = serverFor[PersonlagomService](wire[PersonlagomServiceImpl])

  // Register the JSON serializer registry
  override lazy val jsonSerializerRegistry = PersonlagomSerializerRegistry
  object PersonlagomSerializerRegistry extends JsonSerializerRegistry {
    override def serializers: Seq[JsonSerializer[_]] = CompanyFormats.serializers ++ PersonFormats.serializers
  }
  // Register the PersonLagom persistent entity
  persistentEntityRegistry.register(wire[PersonEntity])
  persistentEntityRegistry.register(wire[CompanyEntity])
}
